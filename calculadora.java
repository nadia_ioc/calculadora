package ioc.xtec.cat.calculadora;

import java.util.Scanner;

/**
 * Classe que defineix el programa Calculadora
 * @author  Nàdia Núñez Berrocal
 * @version 1.0
 * @since 2024.05.04
 */

public class Calculadora {
    
    /**
     * Mètode que actúa com a punt d'entrada del programa
     * Demana a l'usuari que introdueixi el primer número
     * A continuació demana el segon número
     * Mostra les operacions (suma, resta i multiplicació) i crida la class adient per fer-les
     * Després intenta fer la divisió
     * Si la divisió es correcte la mostra i si no, mostra l'excepció
     * @param args  Array de cadenes que conté els arguments passats per paràmetre
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in); //inicia el scanner

        System.out.println("Introdueix el primer número: ");
        double num1 = scanner.nextDouble();

        System.out.println("Introdueix el segon número: ");
        double num2 = scanner.nextDouble();

        System.out.println("Suma: " + sumar(num1, num2));

System.out.println("Resta: " + restar(num1, num2));        
System.out.println("Multiplicacio: " + multiplicar(num1, num2));
  try {
            System.out.println("Divisió: " + Operacions.dividir(num1, num2));
        } catch (ArithmeticException e) {
            System.out.println("Error: " + e.getMessage());
        } finally {
            scanner.close(); //tanca el scanner
        }

        
    }
    
    public static double sumar(double a, double b) {
            return a + b;
	}
    
public static double restar(double a, double b) {
        return a - b;
    }

public static double multiplicar(double a, double b) {
        return a * b;
    }

public static double dividir(double a, double b) throws ArithmeticException {
        if (b == 0) {
            throw new ArithmeticException("El divisor no pot ser zero");
        }
        return a / b;
    }

}
